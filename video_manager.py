#!/usr/bin/env python2
# RedVidEncoder
# Copyright (C) 2016  Red_M ( http://bitbucket.com/Red_M )

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import os
import sys
if sys.version_info < (3, 0):
    reload(sys)
    sys.setdefaultencoding('UTF8')
import time
import datetime
import json
import mimetypes
import traceback
import subprocess
import multiprocessing
from watchdog.observers import Observer as watchdog_observer
from watchdog.events import FileSystemEventHandler as watchdog_file_event_handler

os.chdir('.' or sys.path[0])
current_dir = os.path.join(os.getcwd(),os.sep.join(sys.argv[0].split(os.sep)[0:-1]))
if current_dir.endswith('.'):
    current_dir = current_dir[0:-1]
if sys.argv[0].split(os.sep)[-1] in os.listdir(current_dir):
    print('INFO: Found path')
else:
    print('INFO: Bad path')
    exit()

def debugger(lvl=5,message=''):
        message = str(message)
        levels = {
            0:'FATAL',
            1:'CRITICAL',
            2:'ERROR',
            3:'INFO',
            4:'MESSAGE',
            5:'DEBUG'
        }
        if lvl in levels:
            lvl = levels[lvl]
        if '\n' in message:
            message = message.replace('\n','\n'+str(lvl)+': ')
        print(str(lvl)+': '+message)

def trace_back():
    type_, value_, traceback_ = sys.exc_info()
    ex = traceback.format_exception(type_, value_, traceback_)
    return(''.join(ex))

class VideoFileEventHandler(object):
    def __init__(self,config,queue):
        self.config = config
        self.queue = queue
    
    def process_file(self,file_location,i=0):
        scale = file_location.split(os.sep)[-2]
        if 'format' in self.config['video_transcoding'][scale] and 'extention' in self.config['video_transcoding'][scale]:
            format = self.config['video_transcoding'][scale]['format']
            extention = '.'+self.config['video_transcoding'][scale]['extention']
        else:
            format = 'libvpx'
            extention = '.webm'
        
        def check_video_format(file_location):
            ffprobe = 'ffprobe -show_format -show_streams -loglevel quiet -print_format json'
            cmd = ffprobe.split(' ')
            cmd.append(file_location)
            # debugger(3,cmd)
            try:
                ffprobe_out = json.loads(subprocess.check_output(cmd))
            except Exception as e:
                # debugger(3,trace_back())
                debugger(3,'A file failed to be inspected: '+file_location)
                return()
            for stream in ffprobe_out['streams']:
                if stream['codec_type']=='video':
                    #if not stream['codec_name']=='vp8': # if the target format has changed, this will be wrong because we could be targeting vp8 to convert to another codec.
                    debugger(3,stream['codec_name'])
                    debugger(3,'Queued: '+file_location.split(os.sep)[-1]+' actioning as: '+scale)
                    self.queue.put((file_location,scale))
                    return()
        if not file_location.endswith(extention):
            file_sizes = []
            for sleep_time in xrange(1,16):
                file_sizes.append(os.path.getsize(file_location))
                time.sleep(1)
            if file_sizes[0]==file_sizes[-1]:
                check_video_format(file_location)
            elif i<30:
                time.sleep(float(i)/2.0)
                self.process_file(file_location,i+1)

    def on_any_event(self, event):
        '''Catch-all event handler.

        :param event:
            The event object representing the file system event.
        :type event:
            :class:`FileSystemEvent`
        '''

    def on_created(self, event):
        what = 'directory' if event.is_directory else 'file'
        if what=='file':
            # debugger(3,'Created: '+event.src_path)
            self.process_file(event.src_path)

class ConfigFileEventHandler(object):
    def __init__(self,vid_queue_processing):
        self.vid_queue = vid_queue_processing

    def on_any_event(self, event):
        '''Catch-all event handler.

        :param event:
            The event object representing the file system event.
        :type event:
            :class:`FileSystemEvent`
        '''

    def on_moved(self, event):
        pass

    def on_created(self, event):
        what = 'directory' if event.is_directory else 'file'
        if what=='file':
            if event.src_path.split(os.sep)[-1]=='config.json':
                #RedServ.debugger(3,'Created: '+event.src_path)
                if not os.stat(event.src_path).st_size==0:
                    self.vid_queue.config_reload(event.src_path)

    def on_deleted(self, event):
        pass

    def on_modified(self, event):
        what = 'directory' if event.is_directory else 'file'
        if what=='file':
            if event.src_path.split(os.sep)[-1]=='config.json':
                #RedServ.debugger(3,'Changed: '+event.src_path)
                if not os.stat(event.src_path).st_size==0:
                    self.vid_queue.config_reload(event.src_path)

class VideoQueue(object):
    
    def __init__(self,init_config):
        self.config = init_config
        root_dir = os.path.join(current_dir,'videos')
        for option_folder in self.config['video_transcoding']:
            option_folder_path = os.path.join(root_dir,option_folder)
            if not os.path.exists(option_folder_path):
                os.mkdir(option_folder_path)
            else:
                if os.path.isdir(option_folder_path):
                    pass # error if a file exists instead of a folder.
    
    def get_default_config(self):
        config = {
            'video_transcoding':{
                '720':{ # folder
                    'h_size':'1280', # video height
                    'v_size':'720', # video vertical length
                    'kbps':'7500'
                },
                '1080':{
                    'h_size':'1920',
                    'v_size':'1080',
                    'kbps':'15000'
                },
                '1440':{
                    'h_size':'2560',
                    'v_size':'1440',
                    'kbps':'20000'
                },
                'webm':{
                    'h_size':'0', # zero means auto size
                    'v_size':'0',
                    'kbps':'20000' # zero bitrate turns off bitrate changing
                },
                'all':{
                    'profiles':['720','1080','1440']
                }
            },
            'cpu_core_count':6 #CPU cores to use
        }
        return(config)
    
    def config_reload(self,config):
        f = open(config, 'r')
        self.config = json.load(f)
        f.close()
    
    def process_video(self,file_location,scale,target_file_prepend='',chained=False):
        
        def get_option(profile_config,option):
            default_options = {
                'debug':False,
                'format':'libvpx',
                'extention':'webm',
                'preset':None,
                'quality':'best'
            }
            
            if option in profile_config:
                value = profile_config[option]
            else:
                value = default_options[option]
            return(value)
        
        
        
        if not scale in self.config['video_transcoding']:
            if chained==False:
                self.remove_video(file_location)
            return()
            
        if not os.path.exists(file_location):
            debugger(3,'Deleted '+scale+': '+file_location.split(os.sep)[-1])
            return()
            
        profile_config = self.config['video_transcoding'][scale]
        if 'profiles' in profile_config:
            for profile in profile_config['profiles']:
                if profile in self.config['video_transcoding']:
                    self.process_video(file_location,profile,profile+'_',True)
            if chained==False:
                self.remove_video(file_location)
            return()
        
        video_size = str(profile_config['h_size'])+':'+str(profile_config['v_size'])
        kbps = profile_config['kbps']
        format = get_option(profile_config,'format')
        extention = '.'+get_option(profile_config,'extention')
        quality = get_option(profile_config,'quality')
        core_count = str(int(self.config['cpu_core_count']))
        
        if video_size=='0:0':
            resize_option = ''
        else:
            resize_option = ' -vf scale='+video_size+' -quality '+quality+' -lag-in-frames 16'
        
        if int(kbps)==0:
            bitrate_option = ' '
        else:
            bitrate_option = ' -crf 32 -b:v '+str(kbps)+'K'
        
        
        ffmpeg_start = 'ffmpeg -y -loglevel error -hide_banner -i'
        ffmpeg_args = '-movflags +faststart -sn -c:v '+format+bitrate_option+resize_option+' -threads '+core_count
        preset = get_option(profile_config,'preset')
        if not preset==None:
            ffmpeg_args = ffmpeg_args+' -preset '+preset
        file_name = '.'.join(file_location.split(os.sep)[-1].split('.')[:-1])+extention
        cmd = ffmpeg_start.split(' ')
        cmd.append(file_location)
        cmd.extend(ffmpeg_args.split(' '))
        target_file_location = file_location.split(os.sep)[:-1]
        target_file_location.append(target_file_prepend+file_name)
        cmd.append(os.sep.join(target_file_location))
        debugger(3,'Starting '+scale+': '+file_location.split(os.sep)[-1])
        try:
            if get_option(profile_config,'debug'):
                debugger(3,cmd)
            subprocess.check_output(cmd)
            debugger(3,'Completed '+scale+': '+file_name)
        except Exception as e:
            if isinstance(subprocess.CalledProcessError(0,'',''),type(e)):
                debugger(2,' '.join(e.cmd)+': '+e.output)
        finally:
            if chained==False:
                self.remove_video(file_location)
        
    def remove_video(self,file):
        os.remove(file)



def main(watchdogs,video_queue,vid_queue_processing):
    for observer in watchdogs:
        observer.start()
    debugger(3,'Started, watching for files.')
    while True:
        (file_location,scale) = video_queue.get()
        if not file_location==None:
            vid_queue_processing.process_video(file_location,scale)
        else:
            time.sleep(1)
    

if __name__ == '__main__':
    watchdogs = []
    watchdog_path = os.path.join(current_dir,'videos')
    video_queue = multiprocessing.Queue()
    
    config_path = os.path.join(current_dir,'config.json')
    if os.path.exists(config_path):
        f = open(config_path, 'r')
        config = json.load(f)
        f.close()
    else:
        vid = VideoQueue({'video_transcoding':{}})
        f = open(config_path, 'w')
        f.write(json.dumps(vid.get_default_config(), sort_keys=True,indent=2, separators=(',', ': ')))
        f.close()
        debugger(1,'Error, no config.json found! config.json has been generated for you. Please make sure your settings in config.json are correct.')
        sys.exit(1)
    
    vid_queue_processing = VideoQueue(config)
    
    video_file_event_handler = VideoFileEventHandler(config,video_queue)
    video_event_handler = watchdog_file_event_handler()
    video_event_handler.on_any_event = video_file_event_handler.on_any_event
    video_event_handler.on_created = video_file_event_handler.on_created
    video_video_observer = watchdog_observer()
    video_observer = watchdog_observer()
    video_observer.schedule(video_event_handler, watchdog_path, recursive=True)
    watchdogs.append(video_observer)
    
    config_file_event_handler = ConfigFileEventHandler(vid_queue_processing)
    config_event_handler = watchdog_file_event_handler()
    config_event_handler.on_any_event = config_file_event_handler.on_any_event
    config_event_handler.on_created = config_file_event_handler.on_created
    config_event_handler.on_modified = config_file_event_handler.on_modified
    config_config_observer = watchdog_observer()
    config_observer = watchdog_observer()
    config_observer.schedule(config_event_handler, current_dir, recursive=False)
    watchdogs.append(config_observer)
    try:
        main(watchdogs,video_queue,vid_queue_processing)
    except Exception as e:
        debugger(1,trace_back())
        # sys.exit(1)
    finally:
        for observer in watchdogs:
            if observer.is_alive():
                observer.stop()
                observer.join()
    sys.exit(0)
